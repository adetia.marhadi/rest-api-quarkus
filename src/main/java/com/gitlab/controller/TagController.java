package com.gitlab.controller;

import com.gitlab.dto.Tag;
import com.gitlab.service.TagService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/tag")
public class TagController {

    private static final Logger log = LoggerFactory.getLogger(TagController.class);

    @Inject
    TagService tagService;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Tag saveTag(@Valid Tag tag) {
        return this.tagService.save(tag);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Tag> allTag() {
        return this.tagService.findAll();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Tag tagById(@PathParam("id") Long id) {
        return this.tagService.findById(id);
    }

    @DELETE
    @Path("/{id}")
    public Response deleteById(@PathParam("id") Long id) {
        log.info("tag id = {} will be deleted", id);
        boolean delete = this.tagService.delete(id);
        if (delete) {
            return Response.ok().build();
        }
        return Response.serverError().build();
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response update(@PathParam("id") Long id, @Valid Tag tag) {
        Tag t = this.tagService.update(id, tag);
        if (null == t) {
            return Response.noContent().build();
        }

        return Response.ok(t).build();
    }
}
