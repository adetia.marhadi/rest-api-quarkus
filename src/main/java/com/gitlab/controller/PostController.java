package com.gitlab.controller;

import com.gitlab.dto.Post;
import com.gitlab.dto.Tag;
import com.gitlab.service.PostService;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Set;

@Path("/post")
public class PostController {

    @Inject
    PostService postService;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Post saveTag(@Valid Post post) {
        return this.postService.save(post);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Post> allPost() {
        return this.postService.findAll();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Post tagById(@PathParam("id") Long id) {
        return this.postService.findById(id);
    }

    @DELETE
    @Path("/{id}")
    public Response deleteById(@PathParam("id") Long id) {
        boolean delete = this.postService.delete(id);
        if (delete) {
            return Response.ok().build();
        }
        return Response.serverError().build();
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response update(@PathParam("id") Long id, @Valid Post post) {
        Post p = this.postService.update(id, post);
        if (null == p) {
            return Response.noContent().build();
        }

        return Response.ok(p).build();
    }

    @PUT
    @Path("/{post_id}/tag/{tag_id}")
    public Post addTag(@PathParam("post_id") Long postId, @PathParam("tag_id") Long tagId) {
        return this.postService.addTag(postId, tagId);
    }

    @DELETE
    @Path("/{post_id}/tag")
    public Response addPostTag(@PathParam("post_id") Long postId, @Valid Set<Tag> tags) {
        Post post = this.postService.deletePostTag(postId, tags);
        if (null == post) {
            return Response.noContent().build();
        }
        return Response.ok(post).build();
    }
}
