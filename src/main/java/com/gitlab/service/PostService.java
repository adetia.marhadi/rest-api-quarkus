package com.gitlab.service;

import com.gitlab.dto.Post;
import com.gitlab.dto.Tag;
import com.gitlab.repository.PostRepository;
import com.gitlab.repository.TagRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@ApplicationScoped
public class PostService {

    @Inject
    PostRepository postRepository;

    @Inject
    TagRepository tagRepository;

    @Transactional
    public Post save(Post post) {

        if (!post.getTags().isEmpty()) {

            Set<Tag> tags = new HashSet<>();
            for (Tag tag : post.getTags()) {
                Optional<Tag> labelOptional = this.tagRepository.findByLabelOptional(tag.getLabel());
                if (labelOptional.isPresent()) {
                    tags.add(labelOptional.get());
                } else {
                    tags.add(tag);
                }
            }

            post.getTags().clear();

            for (Tag tag : tags) {
                post.getTags().add(tag);
            }

            this.tagRepository.persist(post.getTags());
        }

        this.postRepository.persist(post);

        return post;
    }

    public List<Post> findAll() {
        return this.postRepository.findAll().list();
    }

    public Post findById(Long id) {
        return this.postRepository.findById(id);
    }

    @Transactional
    public boolean delete(Long id) {
        return this.postRepository.deleteById(id);
    }

    @Transactional
    public Post update(Long id, Post post) {
        Post postShouldBeUpdated = this.postRepository.findById(id);
        if (null == postShouldBeUpdated) {
            return null;
        }

        postShouldBeUpdated.setTitle(post.getTitle());
        postShouldBeUpdated.setContent(post.getContent());

        Set<Tag> updatedTags = new HashSet<>();

        Set<Tag> tags = post.getTags();
        for (Tag tagRequest : tags) {
            Optional<Tag> labelOptional = this.tagRepository.findByLabelOptional(tagRequest.getLabel());
            if (labelOptional.isPresent()) {
                Tag existingTag = labelOptional.get();
                updatedTags.add(existingTag);
            } else {
                updatedTags.add(tagRequest);
            }
        }

        postShouldBeUpdated.getTags().clear();

        for (Tag tag : updatedTags) {
            postShouldBeUpdated.getTags().add(tag);
        }

        this.tagRepository.persist(postShouldBeUpdated.getTags());
        this.postRepository.persist(postShouldBeUpdated);

        return postShouldBeUpdated;
    }

    @Transactional
    public Post addTag(Long postId, Long tagId) {
        Post post = this.postRepository.findById(postId);
        Tag tag = this.tagRepository.findById(tagId);

        post.getTags().add(tag);

        this.postRepository.persist(post);

        return post;
    }

    @Transactional
    public Post deletePostTag(Long id, Set<Tag> tags) {
        Optional<Post> byIdOptional = this.postRepository.findByIdOptional(id);
        if (byIdOptional.isPresent()) {

            Post post = byIdOptional.get();

            Set<Tag> newTag = new HashSet<>();
            for (Tag tag : post.getTags()) {
                for (Tag tag2 : tags) {
                    if (!tag.getLabel().equals(tag2.getLabel())) {
                        newTag.add(tag);
                    }
                }
            }

            post.getTags().clear();

            for (Tag tag : newTag) {
                post.getTags().add(tag);
            }

            this.tagRepository.persist(post.getTags());
            this.postRepository.persist(post);

            return post;
        }

        return null;

    }
}
