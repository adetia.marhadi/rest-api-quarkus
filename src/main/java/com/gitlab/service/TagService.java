package com.gitlab.service;

import com.gitlab.dto.Post;
import com.gitlab.dto.Tag;
import com.gitlab.repository.PostRepository;
import com.gitlab.repository.TagRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@ApplicationScoped
public class TagService {

    @Inject
    TagRepository tagRepository;

    @Inject
    PostRepository postRepository;

    @Transactional
    public Tag save(Tag tag) {

        if (null == tag.getPosts() || tag.getPosts().isEmpty()) {
            this.tagRepository.persist(tag);
        } else {

            Set<Post> posts = tag.getPosts();
            for (Post post : posts) {
                post.getTags().add(tag);
            }

            this.tagRepository.persist(posts.iterator().next().getTags());
            this.postRepository.persist(tag.getPosts());
        }



        return tag;
    }

    public List<Tag> findAll() {
        return this.tagRepository.findAll().list();
    }

    public Tag findById(Long id) {
        return this.tagRepository.findById(id);
    }

    @Transactional
    public boolean delete(Long id) {
        Optional<Tag> byIdOptional = this.tagRepository.findByIdOptional(id);
        if (byIdOptional.isPresent()) {
            Tag tagShouldBeDeleted = byIdOptional.get();

            for (Post postParent : tagShouldBeDeleted.getPosts()) {
                Optional<Post> postOptional = this.postRepository.findByIdOptional(postParent.id);
                if (postOptional.isPresent()) {
                    Post post = postOptional.get();

                    Set<Tag> newTag = new HashSet<>();
                    for (Tag tag : post.getTags()) {
                        if (!tag.getLabel().equals(tagShouldBeDeleted.getLabel())) {
                            newTag.add(tag);
                        }
                    }

                    post.getTags().clear();

                    for (Tag tag : newTag) {
                        post.getTags().add(tag);
                    }

                    this.tagRepository.persist(post.getTags());
                    this.postRepository.persist(post);


                }
            }

            this.tagRepository.delete(tagShouldBeDeleted);

            return true;
        }
        return false;
    }

    @Transactional
    public Tag update(Long id, Tag tag) {
        Tag t = this.tagRepository.findById(id);
        if (null == t) {
            return null;
        }

        t.setLabel(tag.getLabel());

        this.tagRepository.persist(t);

        return t;
    }
}
