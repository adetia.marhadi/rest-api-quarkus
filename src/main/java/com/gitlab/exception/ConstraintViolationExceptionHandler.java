package com.gitlab.exception;

import com.gitlab.dto.ErrorResponse;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.stream.Collectors;

@Provider
public class ConstraintViolationExceptionHandler implements ExceptionMapper<ConstraintViolationException> {
    @Override
    public Response toResponse(ConstraintViolationException e) {
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setResponseCode(Response.Status.BAD_REQUEST.getStatusCode());
        errorResponse.setResponseMessage(e.getConstraintViolations().stream().map(ConstraintViolation::getMessage).collect(
                Collectors.joining(", ")));
        return Response.status(Response.Status.BAD_REQUEST).entity(errorResponse).build();
    }
}
