package com.gitlab.exception;

import com.gitlab.dto.ErrorResponse;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ExceptionHandler implements ExceptionMapper<Exception> {
    @Override
    public Response toResponse(Exception e) {
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setResponseCode(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode());
        errorResponse.setResponseMessage("the request can not be processed");

        return Response.status(Response.Status.BAD_REQUEST).entity(errorResponse).build();
    }
}
