package com.gitlab.repository;

import com.gitlab.dto.Tag;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;

import javax.enterprise.context.ApplicationScoped;
import java.util.Optional;

@ApplicationScoped
public class TagRepository implements PanacheRepository<Tag> {

    public Optional<Tag> findByLabelOptional(String label) {
        return find("label", label).singleResultOptional();
    }
}
