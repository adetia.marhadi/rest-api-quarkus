package com.gitlab.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "tb_post")
public class Post extends PanacheEntity {

    @NotBlank(message = "title may not be blank")
    private String title;
    @NotBlank(message = "content may not be blank")
    private String content;

    @JsonIgnoreProperties(value= {"posts"})
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "tb_post_tag", joinColumns = @JoinColumn(name = "post_id"), inverseJoinColumns = @JoinColumn(name = "tag_id"))
    private Set<Tag> tags = new HashSet<>();

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Set<Tag> getTags() {
        return tags;
    }
}
