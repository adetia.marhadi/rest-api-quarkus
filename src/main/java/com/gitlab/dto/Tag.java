package com.gitlab.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "tb_tag")
public class Tag extends PanacheEntity {

    @NotBlank(message = "label may not be blank")
    private String label;

    @JsonIgnoreProperties(value= {"tags"})
    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "tags")
    private Set<Post> posts = new HashSet<>();

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Set<Post> getPosts() {
        return posts;
    }
}
